﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        if ( Input.GetButton( "Fire1" ) )
        {
            //sets where the mouse clicks to the point on the screen
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );

            //guides the projectiles
            Vector3 FireDirection = clickPoint - this.transform.position;

            //makes each projectile fire straight forward with a magnitude of 1
            FireDirection.Normalize();

            //creates ball
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );

            //sets the speed and direction to our firespeed
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }




    }

}
