﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    
	// Update is called once per frame
	void Update ()
    {
        //this is the main camera
        Camera camera = GameObject.Find( "Main Camera" ).GetComponent<Camera>();
        //sets the raycast
        RaycastHit hit = new RaycastHit();
        //sets point to ground through layermasking
        if ( Physics.Raycast( camera.ScreenPointToRay( Input.mousePosition ), out hit, float.MaxValue, LayerMask.GetMask( "Ground" ) ) )
        {
            //move to destination to set point
            GetComponent<NavMeshAgent>().destination = hit.point;
        }
	}
}
