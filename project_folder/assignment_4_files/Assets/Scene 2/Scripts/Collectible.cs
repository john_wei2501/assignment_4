﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float Radius;

    public void Update()
    {
        //detect if we are colliding with the collectables  
        Collider[] collidingColliders = Physics.OverlapSphere( this.transform.position, Radius );
        for ( int colliderIndex = 0; colliderIndex < collidingColliders.Length; ++colliderIndex )
        {
            //test if the tag is player
            if ( collidingColliders[colliderIndex].tag == "Player" )
            {
                //if collect, destroy the collecatable
                Destroy( this.gameObject );
            }
        }
    }

}
