﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCollectible : MonoBehaviour
{
    public GameObject Collectible;

    private GameObject m_currentCollectible;

    // Update is called once per frame
    void Update()
    {
        // spawn the golden flashy coins in a random location in the level
        if ( m_currentCollectible == null )
        {
            //if no collectable on screen, spawn one, this is the random coordinates
            m_currentCollectible = Instantiate( Collectible, this.transform.GetChild( UnityEngine.Random.Range( 0, this.transform.childCount ) ).position, Collectible.transform.rotation );
        }
    }
}
